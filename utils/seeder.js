// set up a temporary (in memory) database
const Datastore = require('nedb')
const LOG = require('../utils/logger.js')

// require each data file
const users = require('../data/Users.json');
const accounts = require('../data/accounts.json');
const transactions = require('../data/transactions.json')
const category = require('../data/category.json')



// inject the app to seed the data

module.exports = (app) => {
  LOG.info('START seeder.')
  const db = {}


  //Seed Users
  db.users = new Datastore()
  db.users.loadDatabase()

  // insert the sample data into our data store
  db.users.insert(users)

  // initialize app.locals (these objects will be available to our controllers)
  app.locals.users = db.users.find(users)
  LOG.debug(`${app.locals.users.query.length} users seeded`)


  // accounts depends on users

  db.accounts = new Datastore()
  db.accounts.loadDatabase()

  // insert the sample data into our data store
  db.accounts.insert(accounts)

  // initialize app.locals (these objects will be available to our controllers)
  app.locals.accounts = db.accounts.find(accounts)
  LOG.debug(`${app.locals.accounts.query.length} accounts seeded`)


  // accounts depends on users

  db.transactions = new Datastore()
  db.transactions.loadDatabase()

  // insert the sample data into our data store
  db.transactions.insert(transactions)

  // initialize app.locals (these objects will be available to our controllers)
  app.locals.transactions = db.transactions.find(transactions)
  LOG.debug(`${app.locals.transactions.query.length} transactions seeded`)

  // accounts depends on users

  db.category = new Datastore()
  db.category.loadDatabase()

  // insert the sample data into our data store
  db.category.insert(category)

  // initialize app.locals (these objects will be available to our controllers)
  app.locals.category = db.category.find(category)
  LOG.debug(`${app.locals.category.query.length} category seeded`)
}
