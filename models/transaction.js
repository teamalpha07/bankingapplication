/** 
*  Transaction model
*  Describes the each operation in an account.
*
* @author Naga Sai Manoj Goppisetty <S534885@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const transactionSchema = new mongoose.Schema({
    _transactionid: { type: String, required: true , unique: true},
    Acoounttype: { type: String, required: true},
    AccountNumber: { type: Number, required: true, unique: true, default: '123456789' },
    Amount: { type:Number, required: true, default: '0' },
    AvailbleBalance: { type: Number, required: true},
    Categeory:{ type: String, required: true, default: 'entertainment'}
})

module.exports = mongoose.model('transaction', transactionSchema)