/** 
*  Product model
*  Describes the characteristics of each attribute in a product resource.
*
* @author Sharadruthi Beerkuri <druthi.beerukuri@gmail.com>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const CategorySchema = new mongoose.Schema({
  _id: { type: Number, required: true,unique: true },
  categoryname: { type: String, required: true}
})

module.exports = mongoose.model('Category', CategorySchema)
