/** 
*  Account model
*  Describes the characteristics of each attribute in a customer resource.
*
* @author Rishika Reddy Gaddam <s534665@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const AccountSchema = new mongoose.Schema({
  id: { type: Number, required: true, unique: true},
  balance: { type: Number, required: true },
  userid: { type: Number, required: true, unique: true},
  accounttype:{type:String ,enum:['CHECKING','SAVINGS']}
})

module.exports = mongoose.model('Account', AccountSchema)
