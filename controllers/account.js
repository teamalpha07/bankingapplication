/** 
*  Customer controller
*  Handles requests related to customers (see routes)
*
* @author Rishika Reddy Gaddam <rishika.reddy.g5@gmail.com>
*
*/

const express = require('express')
const api = express.Router()
const find = require('lodash.find')
const LOG = require('../utils/logger.js')
const remove = require('lodash.remove')
const notfoundstring = 'account not found';
const Model = require('../models/account.js')

// RESPOND WITH JSON DATA  --------------------------------------------
// GET all JSON

api.get('/findall', (req, res) => {
  res.setHeader('Content-Type', 'application/json')
  const data = req.app.locals.accounts.query
  res.send(JSON.stringify(data))
})

// GET one JSON by ID
api.get('/findone/:id', (req, res) => {
  res.setHeader('Content-Type', 'application/json')
  const id = parseInt(req.params.id)
  const data = req.app.locals.accounts.query
  const item = find(data, { _id: id })
  if (!item) { return res.end(notfoundstring) }
  res.send(JSON.stringify(item))
})

//RESPOND WITH VIEWS  --------------------------------------------

// GET to this controller base URI (the default)
api.get('/', (req, res) => {
  res.render('accounts/index.ejs',
  {
    Data:'accounts',
    PageTitle : 'Accounts Default View',
    title: 'Accounts',
  })
})

api.get('/create', (req, res) => {
  LOG.info(`Handling GET /create${req}`)
  const item = new Model()
  LOG.debug(JSON.stringify(item))
  res.render('accounts/create',
    {
      title: 'Create Account',
      layout: 'layout.ejs',
      account: item,
      Data:'accounts',
      PageTitle : 'Accounts Create View'
    })
})

// GET /delete/:id
api.get('/delete/:id', (req, res) => {
  LOG.info(`Handling GET /delete/:id ${req}`)
  const id = parseInt(req.params.id)
  const data = req.app.locals.accounts.query
  const item = find(data, { _id: id })
  if (!item) { return res.end(notfoundstring) }
  LOG.info(`RETURNING VIEW FOR ${JSON.stringify(item)}`)
  return res.render('accounts/delete.ejs',
    {
      title: 'Delete Account',
      layout: 'layout.ejs',
      accounts: item,
      Data:'accounts',
      PageTitle : 'Accounts Delete View'
    })
})
// GET /details/:id
api.get('/details/:id', (req, res) => {
  LOG.info(`Handling GET /details/:id ${req}`)
  const id = parseInt(req.params.id)
  const data = req.app.locals.accounts.query
  const item = find(data, { _id: id })
  if (!item) { return res.end(notfoundstring) }
  LOG.info(`RETURNING VIEW FOR ${JSON.stringify(item)}`)
  return res.render('accounts/details.ejs',
    {
      title: 'Account Details',
      layout: 'layout.ejs',
      accounts: item,
      Data:'accounts',
      PageTitle : 'Accounts Details View'
    })
})
// GET one
api.get('/edit/:id', (req, res) => {
  LOG.info(`Handling GET /edit/:id ${req}`)
  const id = parseInt(req.params.id)
  const data = req.app.locals.accounts.query
  const item = find(data, { _id: id })
  if (!item) { return res.end(notfoundstring) }
  LOG.info(`RETURNING VIEW FOR${JSON.stringify(item)}`)
  return res.render('accounts/edit.ejs',
    {
      title: 'Edit Account',
      layout: 'layout.ejs',
      account: item,
      Data:'accounts',
      PageTitle : 'Accounts Edit View'
    })
})

// HANDLE EXECUTE DATA MODIFICATION REQUESTS --------------------------------------------

// POST new
api.post('/save', (req, res) => {
  LOG.info(`Handling POST ${req}`)
  LOG.debug(JSON.stringify(req.body))
  const data = req.app.locals.accounts.query
  const item = new Model()
  LOG.info(`NEW ID ${req.body._id}`)
  item._id = parseInt(req.body._id)
  item.balance = req.body.balance
  item.userid = req.body.userid
  item.accounttype = req.body.accounttype
  data.push(item)
  LOG.info(`SAVING NEW Account ${JSON.stringify(item)}`)
  return res.redirect('/accounts')
})

// POST update with id
api.post('/save/:id', (req, res) => {
  LOG.info(`Handling SAVE request ${req}`)
  const id = parseInt(req.params.id)
  LOG.info(`Handling SAVING ID=${id}`)
  const data = req.app.locals.accounts.query
  const item = find(data, { _id: id })
  if (!item) { return res.end(notfoundstring) }
  LOG.info(`ORIGINAL VALUES ${JSON.stringify(item)}`)
  LOG.info(`UPDATED VALUES: ${JSON.stringify(req.body)}`)
  item.balance = req.body.balance
  item.userid = req.body.userid
  item.accounttype = req.body.accounttype
  LOG.info(`SAVING UPDATED customer ${JSON.stringify(item)}`)
  return res.redirect('/accounts')
})

// DELETE id (uses HTML5 form method POST)
api.post('/delete/:id', (req, res) => {
  LOG.info(`Handling DELETE request ${req}`)
  const id = parseInt(req.params.id)
  LOG.info(`Handling REMOVING ID=${id}`)
  const data = req.app.locals.accounts.query
  const item = find(data, { _id: id })
  if (!item) { return res.end(notfoundstring) }
  if (item.isActive) {
    item.isActive = false
    console.log(`Deacctivated item ${JSON.stringify(item)}`)
  } else {
    const item = remove(data, { _id: id })
    console.log(`Permanently deleted item ${JSON.stringify(item)}`)
  }
  return res.redirect('/accounts')
})

module.exports = api
