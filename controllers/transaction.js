/** 
*  Customer controller
*  Handles requests related to customers (see routes)
*
* @author Naga Sai Manoj <s534885@nwmissouri.edu>
*
*/
const express = require('express')
const api = express.Router()
const LOG = require('../utils/logger.js')
const find = require('lodash.find')
const remove = require('lodash.remove')
const notfoundstring = 'Transaction not found'
const Model = require('../models/transaction.js')

// RESPOND WITH JSON DATA  --------------------------------------------

// GET all JSON
api.get('/findall', (req, res) => {
  res.setHeader('Content-Type', 'application/json')
  const data = req.app.locals.transactions.query
  res.send(JSON.stringify(data))
})

// GET one JSON by ID
api.get('/findone/:id', (req, res) => {
  res.setHeader('Content-Type', 'application/json')
  const id = parseInt(req.params.id)
  const data = req.app.locals.transactions.query
  const item = find(data, { _transactionid: id })
  if (!item) { return res.end(notfoundstring) }
  res.send(JSON.stringify(item))
})
// RESPOND WITH VIEWS  --------------------------------------------

// GET to this controller base URI (the default)
api.get('/', (req, res) => {
  res.render('transaction/index.ejs',{
    Data:'transactions',
    PageTitle : 'Transactions Default View',
    title: 'Transactions',
  })
})
api.get('/create', (req, res) => {
  LOG.info(`Handling GET /create${req}`)
  const item = new Model()
  LOG.debug(JSON.stringify(item))
  res.render('transaction/create',
    {
      title: 'Create Transaction',
      layout: 'layout.ejs',
      transaction: item,
      Data:'transactions',
      PageTitle : 'Transactions Create View'
    })
})
// GET /delete/:id
api.get('/delete/:id', (req, res) => {
  LOG.info(`Handling GET /delete/:id ${req}`)
  const id = parseInt(req.params.id)
  const data = req.app.locals.transactions.query
  const item = find(data, { _transactionid: id })
  if (!item) { return res.end(notfoundstring) }
  LOG.info(`RETURNING VIEW FOR ${JSON.stringify(item)}`)
  return res.render('transaction/delete.ejs',
    {
      title: 'Delete Transaction',
      layout: 'layout.ejs',
      transactions: item,
      Data:'transactions',
      PageTitle : 'Transactions Delete View'
    })
})
// GET /details/:id
api.get('/details/:id', (req, res) => {
  LOG.info(`Handling GET /details/:id ${req}`)
  const id = parseInt(req.params.id)
  const data = req.app.locals.transactions.query
  const item = find(data, { _transactionid: id })
  if (!item) { return res.end(notfoundstring) }
  LOG.info(`RETURNING VIEW FOR ${JSON.stringify(item)}`)
  return res.render('transaction/details.ejs',
    {
      title: 'Transaction Details',
      layout: 'layout.ejs',
      transactions: item,
      Data:'transactions',
      PageTitle : 'Transactions Details View'
    })
})
// GET one
api.get('/edit/:id', (req, res) => {
  LOG.info(`Handling GET /edit/:id ${req}`)
  const id = parseInt(req.params.id)
  const data = req.app.locals.transactions.query
  const item = find(data, { _transactionid: id })
  if (!item) { return res.end(notfoundstring) }
  LOG.info(`RETURNING VIEW FOR${JSON.stringify(item)}`)
  return res.render('transaction/edit.ejs',
    {
      title: 'Edit Transaction',
      layout: 'layout.ejs',
      transactions: item,
      Data:'transactions',
      PageTitle : 'Transactions Edit View'
    })
})

// HANDLE EXECUTE DATA MODIFICATION REQUESTS --------------------------------------------

// POST new
api.post('/save', (req, res) => {
  LOG.info(`Handling POST ${req}`)
  LOG.debug(JSON.stringify(req.body))
  const data = req.app.locals.transactions.query
  const item = new Model()
  LOG.info(`NEW ID ${req.body._transactionid}`)
  item._transactionid = parseInt(req.body._transactionid)
  item.Acoounttype = req.body.Acoounttype
  item.AccountNumber = req.body.AccountNumber
  item.Amount = req.body.Amount
  item.AvailbleBalance = req.body.AvailbleBalance
  item.Categeory = req.body.Categeory
  data.push(item)
  LOG.info(`SAVING NEW customer ${JSON.stringify(item)}`)
  return res.redirect('/users')
})

// POST update with id
api.post('/save/:id', (req, res) => {
  LOG.info(`Handling SAVE request ${req}`)
  const id = parseInt(req.params.id)
  LOG.info(`Handling SAVING ID=${id}`)
  const data = req.app.locals.transactions.query
  const item = find(data, { _transactionid: id })
  if (!item) { return res.end(notfoundstring) }
  LOG.info(`ORIGINAL VALUES ${JSON.stringify(item)}`)
  LOG.info(`UPDATED VALUES: ${JSON.stringify(req.body)}`)
  item._transactionid = parseInt(req.body._transactionid)
  item.Acoounttype = req.body.Acoounttype
  item.AccountNumber = req.body.AccountNumber
  item.Amount = req.body.Amount
  item.AvailbleBalance = req.body.AvailbleBalance
  item.Categeory = req.body.Categeory
  LOG.info(`SAVING UPDATED customer ${JSON.stringify(item)}`)
  return res.redirect('/transactions')
})

// DELETE id (uses HTML5 form method POST)
api.post('/delete/:id', (req, res) => {
  LOG.info(`Handling DELETE request ${req}`)
  const id = parseInt(req.params.id)
  LOG.info(`Handling REMOVING ID=${id}`)
  const data = req.app.locals.transactions.query
  const item = find(data, { _transactionid: id })
  if (!item) { return res.end(notfoundstring) }
  if (item.isActive) {
    item.isActive = false
    console.log(`Deacctivated item ${JSON.stringify(item)}`)
  } else {
    const item = remove(data, { _transactionid: id })
    console.log(`Permanently deleted item ${JSON.stringify(item)}`)
  }
  return res.redirect('/transactions')
})

module.exports = api
