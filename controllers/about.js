const express = require('express')
const api = express.Router()
const LOG = require('../utils/logger.js')

// Specify the handler for each required combination of URI and HTTP verb

// HANDLE VIEW DISPLAY REQUESTS --------------------------------------------

api.get('/', (req, res) => {
  LOG.info(`Handling GET /dev1 ${req}`)
  res.render('about/index.ejs',
    {
      layout: 'layout.ejs',
      Data:'about',
      PageTitle : 'About View',
      title : 'About'
    })
})
// GET dev1
api.get('/dev1', (req, res) => {
  LOG.info(`Handling GET /dev1 ${req}`)
  res.render('about/dev1/index.ejs',
    {
      layout: 'layout.ejs',
      Data:'about',
      PageTitle : 'Developer 1 View',
      title : 'Developer 1'
    })
})



// GET dev2
api.get('/dev2', (req, res) => {
  LOG.info(`Handling GET /dev2 ${req}`)
  res.render('about/dev2/index.ejs',
    {
      layout: 'layout.ejs',
      Data:'about',
      PageTitle : 'Developer 2 View',
      title : 'Developer 2'
    })
})



// GET dev3
api.get('/dev3', (req, res) => {
  LOG.info(`Handling GET /dev3 ${req}`)
  res.render('about/dev3/index.ejs',
    {
      layout: 'layout.ejs',
      Data:'about',
      PageTitle : 'Developer 3 View',
      title : 'Developer 3'
    })
})

// GET dev4
api.get('/dev4', (req, res) => {
  LOG.info(`Handling GET /dev4 ${req}`)
  res.render('about/dev4/index.ejs',
    {
      layout: 'layout.ejs',
      Data:'about',
      PageTitle : 'Developer 4 View',
      title : 'Developer 4'
    })
})



module.exports = api
