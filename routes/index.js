/**
 * @index.js - manages all routing
 *
 * router.get when assigning to a single request
 * router.use when deferring to a controller
 *
 * @requires express
 */

const express = require('express')
const LOG = require('../utils/logger.js')

LOG.debug('START routing')
const router = express.Router()

// Manage top-level request first
router.get('/', (req, res, next) => {
  LOG.debug('Request to /')
  LOG.info(`Handling GET ${ process.env.PORT}`)
  res.render('index.ejs', { title: 'Welcome',
  Hostname: req.hostname,
  PortName: getport(req.hostname),
  PageTitle: 'Default View'
})
})
// Defer path requests to a particular controller
router.use('/about', require('../controllers/about.js'))
router.use('/users', require('../controllers/users.js'))
router.use('/accounts', require('../controllers/account.js'))
router.use('/transactions', require('../controllers/transaction.js'))
router.use('/category', require('../controllers/category.js'))
LOG.debug('END routing')
function getport(Hostname)
{
  if(Hostname==='localhost')
  return process.env.PORT;
  else 
  return '443';
}
module.exports = router
